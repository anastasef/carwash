# Car Wash - Автомойка

## Задача

Написать программу для автомойки.

Одновременно может обслуживаться только один автомобиль. Есть список услуг, который могут выбрать клиенты. Каждая услуга выполняется N минут.

Написать программу, в которой клиенты могут записываться на мойку и смотреть свое место в очереди или сколько им ждать своего времени. Без предварительной записи.

Интерфейс по желанию, если без интерфейса использовать сваггер для запросов. Для хранения данных использовать PostgreSQL.


## Перед запуском

Перейти в корневую директорию проекта и запустить скрипт `create_db_user_and_database.sh`.

## Swagger UI

<http://localhost:8080/swagger-ui.html#/car-wash-api>

* * *

## Текущее состояние

Реализовано:

- БД с миграциями, маппинг сущностей
- Рабочий эндпоинт `GET /api/estimate`. Логика для случаев, когда:
	- Никого нет в очереди
	- Заявки в очереди просрочены
	- Последняя заявка позже, чем сейчас
- Рабочий эндпоинт - `POST /api/new`
	- Создание заявки
		- Дубликаты в списке услуг игнорируются
		- Валидация: номер машины и услуги не могут быть пустыми
	- Расчёт времени ожидания своей очереди относительно текущего времени
	
Что хотелось бы реализовать:

- Обработку ошибок
- Валидацию DTO
- Тесты


* * *

## Структура базы данных

![](CarWash_DB_1.png)

`clients` -- таблица с клиентами. Каждому клиенту соответствует номер машины. При добавлении заявки сначала происходит поиск клиента по номеру. У каждого клиента есть множество записей на мойку.

`orders` -- таблица с записями на услуги. Записи при создании присваивается время начала и время её окончания. Время понадобится для расчёта времени ожидания для клиента. Подразумевается, что клиенту не нужно ожидать просроченные записи.

`services` -- таблица с услугами автомойки. Для каждой услуги указана её длительность в минутах.

`order_to_service` -- Соединительная таблица для пула услуг по каждой заявке. Id услуги и заявки являются составным первичным ключом. Можно было бы предусмотреть дублирование услуг в каждой заявке (создав ещё один столбец с количеством для услуги), но, чтобы не усложнять, я не стала так делать.