#!/bin/bash
cd /tmp
sudo su postgres <<EOF
psql -c "CREATE USER carwashadmin WITH PASSWORD 'pass';"
createdb carwashdb
psql -c "GRANT ALL PRIVILEGES ON DATABASE carwashdb TO carwashadmin;"
echo "Postgres user carwashadmin and database carwashdb created."
EOF