package localhost.carwash.controllers;


import io.swagger.annotations.ApiResponse;
import localhost.carwash.models.dto.NewOrderResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import localhost.carwash.exceptions.ApiException;
import localhost.carwash.models.dto.NewOrderDto;
import localhost.carwash.models.dto.QueueEstimatesResponseDto;
import localhost.carwash.services.CarWashService;

@RestController
@RequestMapping("/api")
public class CarWashController {

	private final CarWashService carWashService;

	@Autowired
	public CarWashController(CarWashService carWashService) {
		this.carWashService = carWashService;
	}

	/**
	 * Записаться в очередь
	 * @param orderDto Список (множество) названий услуг
	 * @return
	 */
	@PostMapping("/new")
	@ApiOperation(
			value = "Записаться в очередь",
			response = QueueEstimatesResponseDto.class
	)
	@ApiResponses({
		@ApiResponse(code = 422, message = "Ошибки валидации: номер машины и услуги не могут быть пустыми")
	})
	ResponseEntity<?> enqueueNewOrder(@RequestBody NewOrderDto orderDto) {

		if (orderDto.getVehicleNumber().isEmpty()) {
			return new ResponseEntity<>("Номер машины не может быть пустым", HttpStatus.UNPROCESSABLE_ENTITY);
		}

		NewOrderResponseDto response;
		try {
			response = carWashService.enqueueNewOrder(orderDto);
		} catch (ApiException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}



	/**
	 * Посмотреть сколько клиентов в очереди и время ожидания не записываясь
	 * @return
	 */
	@GetMapping("/estimate")
	@ApiOperation(
			value = "Посмотреть своё место в очереди и время ожидания не записываясь",
			response = QueueEstimatesResponseDto.class
	)
	ResponseEntity<?> requestOrderTimeEstimates() {

		return new ResponseEntity<>(carWashService.calculateTimeEstimates(), HttpStatus.OK);
	}

}
