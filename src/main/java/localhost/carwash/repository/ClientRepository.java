package localhost.carwash.repository;

import localhost.carwash.models.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    public Optional<Client> findOptionalByVehicleNumber(String vehicle);

}
