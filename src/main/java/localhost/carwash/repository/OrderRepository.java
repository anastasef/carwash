package localhost.carwash.repository;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import localhost.carwash.models.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

	/**
	 * Найти самую позднюю запись
	 */
    public Order findTopByOrderByEndAtDesc();

	/**
	 * Подсчитать кол-во заказов начиная с некоторого времени
	 */
	public long countByEndAtAfter(LocalDateTime dateTime);
}
