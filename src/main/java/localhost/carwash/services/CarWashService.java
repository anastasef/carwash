package localhost.carwash.services;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import localhost.carwash.models.Client;
import localhost.carwash.models.dto.NewOrderResponseDto;
import localhost.carwash.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import localhost.carwash.exceptions.ApiException;
import localhost.carwash.models.CarService;
import localhost.carwash.models.Order;
import localhost.carwash.models.dto.NewOrderDto;
import localhost.carwash.models.dto.QueueEstimatesResponseDto;
import localhost.carwash.repository.CarServiceRepository;
import localhost.carwash.repository.OrderRepository;

@Service
public class CarWashService {

    private Order lastOrder;
    private final OrderRepository orderRepo;
    private final CarServiceRepository serviceRepo;
    private final ClientRepository clientRepo;

    @Autowired
    public CarWashService(OrderRepository orderRepo,
                          CarServiceRepository serviceRepo,
                          ClientRepository clientRepo) {
        this.orderRepo = orderRepo;
        this.serviceRepo = serviceRepo;
        this.clientRepo = clientRepo;
        lastOrder = orderRepo.findTopByOrderByEndAtDesc();
    }

    public QueueEstimatesResponseDto calculateTimeEstimates() {
        return createEstimatesResponseDto(orderRepo.countByEndAtAfter(LocalDateTime.now()));
    }

    /**
     * Создать новую заявку.<br>
     * - Если в списке услуг по заявке есть такие, которых нет в БД,
     * то они игнорируются.<br>
     * - если время последней заявки < сейчас, то начало новой заявки = сейчас;<br>
     * иначе оно высчитывается относительно окончания последней заявки
     */
    public NewOrderResponseDto enqueueNewOrder(NewOrderDto orderDto) throws RuntimeException {

        Optional<Client> clientMaybe = clientRepo.findOptionalByVehicleNumber(orderDto.getVehicleNumber());

        Client client = (clientMaybe.isEmpty())
                ? new Client(orderDto.getVehicleNumber())
                : clientMaybe.get();

    	Order order = new Order();

    	// TODO refactoring:
    	// вынести в отдельный сервис для order
        // Луковая архитектура - сделать юзкейс, который будет вызывать сервисы
        // Не делать селект к базе в цикле
        // Сделать проверку на кол-во входных параметров.
        // Если их меньше/больше, чем найденных сущностей, то кидать исключение
        Set<Integer> servicesIds = orderDto.getServicesIds();
        servicesIds
    		.forEach(id -> {
    			serviceRepo.findById(id)
    			.ifPresent(order::addService);
    		});
        // TODO serviceRepo.findAllById(servicesIds)
        
        if (order.getServices().isEmpty()) throw new ApiException("Нет добавленных услуг либо они некорректные");
        order.setBeginTime(calcBeginTime());
    	order.setEndTime(calcEndTime(order));
    	orderRepo.save(order);

        this.lastOrder = order;
        client.addOrder(order);
        clientRepo.save(client);
        
        return createNewOrderResponseDto(order);
    }

    private LocalDateTime calcBeginTime() {

        return (lastOrder == null || lastOrder.getEndTime().isBefore(LocalDateTime.now()) ) ?
                LocalDateTime.now()
                : lastOrder.getEndTime();
    }

    private LocalDateTime calcEndTime(Order order) {

        Integer duration = order.getServices().stream()
                .map(CarService::getDuration)
                .reduce(0, Integer::sum);
        
        return order.getBeginTime()
                .plusMinutes(duration);
    }

    /**
     * Считает время ожидания своей очереди и возвращает его в отформатированном виде
     * @return "X ч ХХ мин" или "ХХ мин"
     */
    private String calculateTimeToWait() {

        if (this.lastOrder == null || this.lastOrder.getEndTime().isBefore(LocalDateTime.now())) return "0 мин";

        Duration duration = Duration.between(LocalDateTime.now(), this.lastOrder.getEndTime());
        long hours = duration.toHours();
        long minutes = duration.toMinutes() % 60;

        return (hours == 0) ?
                String.format("%d мин", minutes)
                : String.format("%d ч %d мин", hours, minutes);

    }

    private QueueEstimatesResponseDto createEstimatesResponseDto(long clientsBefore) {
        
        QueueEstimatesResponseDto res = new QueueEstimatesResponseDto();
        res.setClientsBefore(clientsBefore);
        res.setFormattedTimeLeft(calculateTimeToWait());
        
        return res;
    }
    
    private NewOrderResponseDto createNewOrderResponseDto(Order order) {

    	NewOrderResponseDto res = new NewOrderResponseDto();
    	res.setOrderId(order.getId());
    	res.setFormattedTimeLeft(calculateTimeToWait());

    	return res;
    }
}
