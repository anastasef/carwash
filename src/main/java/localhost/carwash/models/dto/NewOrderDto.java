package localhost.carwash.models.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

@ApiModel(description = "DTO новой заявки")
public class NewOrderDto {

	@ApiModelProperty(required = true)
    private String vehicleNumber = "";

    @ApiModelProperty(dataType = "Set", value = "services", required = true)
    private Set<Integer> servicesIds;

    public void setVehicle(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleNumber() {
        return vehicleNumber;
    }

    public Set<Integer> getServicesIds() {
        return servicesIds;
    }

	public void setServicesIds(Set<Integer> servicesIds) {
		this.servicesIds = servicesIds;
	}
    
    
}
