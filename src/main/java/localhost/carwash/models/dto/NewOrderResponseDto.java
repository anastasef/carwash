package localhost.carwash.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Номер заявки и сколько ждать своего времени
 */
@ApiModel(description = "DTO номера созданной заявки и сколько ждать своего времени")
public class NewOrderResponseDto {

    @ApiModelProperty(name = "orderId", value = "orderId", required = true)
    private long orderId;

    @ApiModelProperty(name = "timeLeft", value = "timeLeft", required = true)
    @JsonProperty("timeLeft")
    private String formattedTimeLeft;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getFormattedTimeLeft() {
        return formattedTimeLeft;
    }

    public void setFormattedTimeLeft(String formattedTimeLeft) {
        this.formattedTimeLeft = formattedTimeLeft;
    }

}
