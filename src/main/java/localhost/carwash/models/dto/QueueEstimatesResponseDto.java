package localhost.carwash.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Сколько клиентов в очереди и сколько ждать своего времени
 */
@ApiModel(description = "DTO места в очереди и сколько ждать своего времени")
public class QueueEstimatesResponseDto {

    @ApiModelProperty(name = "clientsBefore", value = "clientsBefore", required = true)
    private long clientsBefore;

    @ApiModelProperty(value = "timeLeft", required = true)
    @JsonProperty("timeLeft")
    private String formattedTimeLeft;

    public long getClientsBefore() {
		return clientsBefore;
	}

	public void setClientsBefore(long clientsBefore) {
		this.clientsBefore = clientsBefore;
	}

	public String getFormattedTimeLeft() {
		return formattedTimeLeft;
	}

	public void setFormattedTimeLeft(String formattedTimeLeft) {
		this.formattedTimeLeft = formattedTimeLeft;
	}

}
