package localhost.carwash.models;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqOrderGen")
    @GenericGenerator(
            name = "seqOrderGen",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "orders_order_id_seq"),
				    @Parameter(name = "initial_value", value = "1"),
				    @Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "order_id")
    private long id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "order_to_service",
            joinColumns = @JoinColumn(name="order_id"),
            inverseJoinColumns = @JoinColumn(name="service_id", insertable=false, updatable=false))
    private Set<CarService> carServices = new HashSet<>();

    @Column(name = "begin_at")
    private LocalDateTime beginAt;

    @Column(name = "end_at")
    private LocalDateTime endAt;



    public long getId() {
        return id;
    }

    public Set<CarService> getServices() {
        return carServices;
    }

    public void setServices(Set<CarService> carServices) {
        this.carServices = carServices;
    }
    
    public void addService(CarService carService) {
    	this.carServices.add(carService);
    }

    public LocalDateTime getBeginTime() {
        return beginAt;
    }

    public void setBeginTime(LocalDateTime beginTime) {
        this.beginAt = beginTime;
    }

    public LocalDateTime getEndTime() {
        return endAt;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endAt = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (beginAt != null ?
                !beginAt.equals(order.beginAt)
                : order.beginAt != null) return false;
        return endAt != null ?
                endAt.equals(order.endAt)
                : order.endAt == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (beginAt != null ? beginAt.hashCode() : 0);
        result = 31 * result + (endAt != null ? endAt.hashCode() : 0);
        return result;
    }
}
