package localhost.carwash.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "clients")
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqClientGen")
	@GenericGenerator(
			name = "seqClientGen",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequence_name", value = "clients_client_id_seq"),
					@org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
					@org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
			}
	)
	@Column(name = "client_id")
	private long id;

	@Column(name = "vehicleNumber")
	private String vehicleNumber;

	@OneToMany(
			cascade = CascadeType.ALL,
			orphanRemoval = true
	)
	@JoinColumn(name = "client_id")
	private Set<Order> orders = new HashSet<>();

	public Client() {
		super();
	}

	public Client(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public long getId() {
		return id;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Set<Order> getOrders() {
		return orders;
	}

	public void addOrder(Order order) {
		this.orders.add(order);
	}

	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
}
