package localhost.carwash.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "services")
public class CarService {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqServiceGen")
    @GenericGenerator(
            name = "seqServiceGen",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "services_service_id_seq"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "service_id")
    private int id;

    @Column(name = "service_id", insertable=false, updatable=false)
    private String name;

    @Column(name = "duration")
    private int duration; // in minutes

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarService carService = (CarService) o;

        if (id != carService.id) return false;
        if (duration != carService.duration) return false;
        return name.equals(carService.name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + duration;
        return result;
    }
}
