CREATE TABLE clients (
  client_id BIGSERIAL PRIMARY KEY,
  vehicle_number VARCHAR(255) UNIQUE NOT NULL
);

ALTER TABLE orders ADD COLUMN client_id bigint REFERENCES clients (client_id) ON DELETE CASCADE;