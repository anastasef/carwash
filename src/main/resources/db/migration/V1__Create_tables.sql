CREATE TABLE clients (
  client_id BIGSERIAL PRIMARY KEY,
  vehicle_number VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE orders (
  order_id BIGSERIAL PRIMARY KEY,
  client_id BIGINT REFERENCES clients (client_id) ON DELETE CASCADE,
  begin_at TIMESTAMP NOT NULL,
  end_at TIMESTAMP NOT NULL
);

CREATE TABLE services (
  service_id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  duration INT NOT NULL -- in minutes
);

CREATE TABLE order_to_service (
  order_id BIGINT REFERENCES orders (order_id) ON DELETE CASCADE,
  service_id INT REFERENCES services (service_id) ON DELETE CASCADE,
  CONSTRAINT order_service_pkey PRIMARY KEY (order_id, service_id)
);