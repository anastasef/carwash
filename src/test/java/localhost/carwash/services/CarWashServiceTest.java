package localhost.carwash.services;

import localhost.carwash.models.Order;
import localhost.carwash.models.dto.QueueEstimatesResponseDto;
import localhost.carwash.repository.OrderRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarWashServiceTest {

	@Mock
	private OrderRepository orderRepo;

	@InjectMocks
	private CarWashService carWashService;


	// Гарантирует, что в DTO.clientsBefore запишется неизменённый ответ из репозитория
	@Test
	@DisplayName("0 клиентов в очереди для первого клиента")
	void firstClient_HasZeroClientsBefore() {
		when(orderRepo.countByEndAtAfter(any(LocalDateTime.class))).thenReturn(0L);
		QueueEstimatesResponseDto responseDto = carWashService.calculateTimeEstimates();
		assertThat(responseDto.getClientsBefore()).isEqualTo(0L);
	}

	// Гарантирует, что в DTO.formattedTimeLeft запишется ответ из приватного метода calculateTimeToWait()
	// Корректная работа для кейса, когда приватное св-во carWashService.lastOrder = null
	@Test
	@DisplayName("Время ожидания = 0 мин для первого клиента")
	void firstClient_HasZeroMinutesToWait() {
		QueueEstimatesResponseDto responseDto = carWashService.calculateTimeEstimates();
		assertThat(responseDto.getFormattedTimeLeft()).isEqualTo("0 мин");
	}

	// Corner case для lastOrder.endTime:
	// Гарантирует корректную работу со свойством carWashService.lastOrder.endTime < now
	@Test
	@DisplayName("Время ожидания = 0 мин, когда нет активных заявок")
	void whenLastOrderIsBeforeNow_ReturnsZeroMinutesToWait() {
		Order lastOrder = new Order();
		lastOrder.setEndTime(LocalDateTime.now().minusMinutes(60));
		ReflectionTestUtils.setField(carWashService, "lastOrder", lastOrder);

		QueueEstimatesResponseDto responseDto = carWashService.calculateTimeEstimates();
		assertThat(responseDto.getFormattedTimeLeft()).isEqualTo("0 мин");
	}

	// Corner case для lastOrder.endTime:
	// Гарантирует корректную работу со свойством carWashService.lastOrder.endTime > now, когда кол-во часов = 0
	@Test
	@DisplayName("Время ожидания в минутах, когда последняя заявка закончится через несколько минут")
	void whenLastOrderIsAfterNowInMinutes_ReturnsMinutesToWait() {
		Order lastOrder = new Order();
		lastOrder.setEndTime(LocalDateTime.now().plusMinutes(10));
		ReflectionTestUtils.setField(carWashService, "lastOrder", lastOrder);

		QueueEstimatesResponseDto responseDto = carWashService.calculateTimeEstimates();
		assertThat(responseDto.getFormattedTimeLeft()).isEqualTo("9 мин");
	}

	// Corner case для lastOrder.endTime:
	// Гарантирует корректную работу со свойством carWashService.lastOrder.endTime > now, когда кол-во часов > 0
	@Test
	@DisplayName("Время ожидания в часах и минутах, когда последняя заявка закончится через несколько часов и минут")
	void whenLastOrderIsAfterNowInHoursMinutes_ReturnsHoursMinutesToWait() {
		Order lastOrder = new Order();
		lastOrder.setEndTime(LocalDateTime.now().plusMinutes(80));
		ReflectionTestUtils.setField(carWashService, "lastOrder", lastOrder);

		QueueEstimatesResponseDto responseDto = carWashService.calculateTimeEstimates();
		assertThat(responseDto.getFormattedTimeLeft()).isEqualTo("1 ч 19 мин");
	}

	// TODO calcEndTime(Order order) : Расчёт времени окончания заявки - сумма длительности услуг заявки
	// 0 услуг = 0 минут
	// 1 услуга 10 мин = order.beginTime + 10 мин
	// 3 услуги = сумма услуг


	// TODO calcBeginTime() : Расчёт времени начала заявки
	// lastOrder == null --> now
	// lastOrder.getEndTime() < now --> now
	// lastOrder.getEndTime() > now --> lastOrder.getEndTime()

	// TODO enqueueNewOrder : Создание новой заявки
	// Сделать после рефакторинга: тесты для
	//      - calcBeginTime(),
	//      - calcEndTime(Order order),
	//      - createNewOrderResponseDto(Order order)

}

